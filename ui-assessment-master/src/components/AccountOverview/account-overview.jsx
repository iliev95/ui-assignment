import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHardHat } from '@fortawesome/free-solid-svg-icons';
import './../AccountOverview/account-overview.css';
import AccountOverviewContainer from '../AccountOverviewContainer/account-overview-container';

export const AccountOverview = ({data}) => {
  console.log(data);

  return (
    <div className="AccountOverview">
      <FontAwesomeIcon icon={faHardHat} />
      Build your components here 
      <AccountOverviewContainer uploadsCount={0} linesCount={0}/>
    </div> 
  )
}

export default AccountOverview;