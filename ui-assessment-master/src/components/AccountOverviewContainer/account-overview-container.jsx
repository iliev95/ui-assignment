import React from 'react';
import PercentageDifference from '../UtilityComponents/DifferenceComponent';
import './../AccountOverview/account-overview.css';
import { MailIconWrapper, InfoIconWrapper, UploadIconWrapper, InfoWrapper, HeaderFlexwrapper, FlexWrapper, HalfWrapperFirst, HalfWrapperSecond, InternalContainer, StyledContainer, StyledAvatar } from './container-styles'
import { faUpload, faInfoCircle, faEnvelope } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import PropTypes from 'prop-types';

export const AccountOverviewContainer = ({ uploadsCount, linesCount }) => {

  return (
    <StyledContainer>
      <HeaderFlexwrapper>
        <h2 className={'overview-title'}>Account Overview</h2>
        <InfoWrapper className={'example'}>
          <p className={'feefo-title'}>YOUR FEEFO SUPPORT CONTACT</p>
          <StyledAvatar>
            <h3 className={'avatar-letter'}>S</h3>
          </StyledAvatar>
          <MailIconWrapper>
            <FontAwesomeIcon className={'mail-icon'} icon={faEnvelope} />
          </MailIconWrapper>
          <p className={'contact-info'}>support@feefo.com 020 3362 4208</p>
          <p className={'support-class'}>Support</p>
        </InfoWrapper>
      </HeaderFlexwrapper>

      <InternalContainer>
        <UploadIconWrapper>
          <FontAwesomeIcon className={'upload-icon'} icon={faUpload} />
        </UploadIconWrapper>
        <p className={'status-update'}>You had {uploadsCount} uploads and {linesCount} lines added.</p>
        <InfoIconWrapper>
          <FontAwesomeIcon className={'info-icon'} icon={faInfoCircle} />
        </InfoIconWrapper>
        <FlexWrapper>
          <HalfWrapperFirst>
            <PercentageDifference className={'diff1'} percentageDifference={0} factor={'Upload Success'} />
          </HalfWrapperFirst>
          <HalfWrapperSecond>
            <PercentageDifference className={'diff2'} percentageDifference={0} factor={'Lines Saved'} />
          </HalfWrapperSecond>
        </FlexWrapper>
      </InternalContainer>
    </StyledContainer>
  )
} 

AccountOverviewContainer.propTypes = {
  uploadsCount: PropTypes.number.isRequired,
  linesCount: PropTypes.number.isRequired
}

export default AccountOverviewContainer;