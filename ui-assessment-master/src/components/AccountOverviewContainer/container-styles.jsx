import styled from 'styled-components';
import { rem } from 'polished';

export const StyledContainer = styled.div`
position: absolute; 
display:block;
width: 50%;
height: 50%;
background-color: #e9ecf2;
margin-top: 12%;  
margin-left: 25%;
border-radius: ${rem('3px')};  

&& .overview-title { 
    postiion:relative; 
    margin-top: 7%;
    margin-left: 6%;
} 
`;

export const InternalContainer = styled.div`
position: relative;
margin: 0 auto; 
top: 10%;
width: 90%;
height: 45%;
background-color:white; 
border-radius:${rem('3px')}; 

&& .status-update { 
    postiion: relative;
    margin-right: 55%;
    margin-top:4.5%;
    color: grey;
} 
`;

export const UploadIconWrapper = styled.div`   
position: relative;     
margin-left: 1px; 
left: -45%;
top: 7% !important; 

&& .upload-icon {  
    color: #34bdeb;  
}
`;

export const InfoIconWrapper = styled.div`   
position: relative;     
margin-left: 1px; 
left: 46%;
margin-top: -11%; 

&& .info-icon {  
    color: grey;  
}
`;

export const FlexWrapper = styled.div` 
position: absolute;
display: flex;
flex-direction: row;
width: 100%;
height: 45%;
bottom:0;
border-top: ${rem('3px')} solid #e9ecf2;
`;

export const HalfWrapperFirst = styled.div` 
position: relative; 
width: 50%;
float:left; 
border-right: ${rem('3px')} solid #e9ecf2;
`;

export const HalfWrapperSecond = styled.div` 
position: relative;
width: 50%;
float:right;  

&& .factor{ 
    margin-right: 7% !important;
}

`;

export const HeaderFlexwrapper = styled.div`
display: flex;
flex-direction: row; 
gap: 11%;
`;


export const InfoWrapper = styled.div`
width: 50%; 

&& .support-class { 
    color: black; 
    margin-top: -12.5%;
    margin-right: 53%;
    font-weight: bold;
    font-size: ${rem('14px')};
}

&& .contact-info { 
    margin-top: -5.5%;
    margin-left: 8%; 
    font-weight: bold;
    font-size: ${rem('14px')};
    color: grey;
}

&& .feefo-title {  
   margin-top: 30px; 
   margin-left: -36%;
   font-weight: bold;
   color: grey; 
}`;

export const MailIconWrapper = styled.div`  
margin-top: -6%;
margin-right: 61%;

&& .mail-icon { 
    color: grey;
} 

`;

export const StyledAvatar = styled.div`
position: relative;
width: 13%;
height: 40%; 
background-color: #fcc603; 
border-radius: ${rem('5px')};

&& .avatar-letter{ 
    position: relative;
    top: 25%;
}

`;

