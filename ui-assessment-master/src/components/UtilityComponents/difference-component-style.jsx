import styled from "styled-components";

export const StyledDifferenceWrapper = styled.div` 

&& .perc-diff {  
    color: green; 
    margin-left: -80%; 
    margin-top: 5px;
    font-size: 30px;
    font-weight: 500;
    line-height: 1.2;
}

&& .factor {
    position: relative;
    margin-top: -33px;
    margin-left: -60%; 
    color: grey;
    font-size: 15px;
    font-weight: 500;
    line-height: 1.2; 
}`;
