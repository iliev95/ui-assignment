import {React} from 'react';
import PropTypes from 'prop-types';
import { StyledDifferenceWrapper } from './difference-component-style';

const PercentageDifference = ({percentageDifference, factor}) => {    

    return (
    <StyledDifferenceWrapper>
        <p className={'perc-diff'}>{percentageDifference}%</p>
        <p className={'factor'}>{factor}</p>
    </StyledDifferenceWrapper>
    )}

PercentageDifference.propTypes = {
percentageDifference: PropTypes.number.isRequired,
factor: PropTypes.string.isRequired
}

export default PercentageDifference;